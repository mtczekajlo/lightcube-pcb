# LightCube Hardware

## Requirements

KiCad 6

```bash
sudo dnf install kicad kicad-packages3d kicad-doc
```

KiBot

```bash
sudo dnf install Xvfb xdotool -y
pip install --no-compile kibot
```

JLCPCB plugin

<https://github.com/Bouni/kicad-jlcpcb-tools>

## Schematics

Schematics are plotted in CI/CD - get the latest pipeline [here](https://gitlab.com/mtczekajlo/lightcube-hardware/pipelines/latest).
